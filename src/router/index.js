// 导入创建路由的方法
import {createRouter,createWebHashHistory} from "vue-router";
import {CONFIG} from "../config/index.js";
import userRoutes from "./user.js";

// 使用路由懒加载的方式导入组件
// const User=()=>import("../views/User.vue")
const Login=()=>import("../view/Login.vue")
const Layout=()=>import("../view/layout/Layout.vue")

// 定义用户相关的路由映射







// 定义路由映射
// 2. 定义一些路由
// 每个路由都需要映射到一个组件。
const routes = [
    {
        path: "/login",
        component: Login,
    },
    {
        path: "/",
        component: Layout,
    },
    userRoutes
]

// 3. 创建路由实例并传递 `routes` 配置
// 你可以在这里输入更多的配置，但我们在这里
const router = createRouter({
    history: createWebHashHistory(),
    routes,
})


// 定义一个全局的守卫，去判断请求链接有没有token字段
router.beforeEach(
    (to, from, next) => {
        // 1. 访问login,本地token ==> next()
        // 2. 访问的还是token 本地token ===> next("/")
        // 3. 访问非login, 本地token ===> next()
        // 4. 访问的不是login ,本地token ==> next("/login")
        // 拿到访问路径
        const  toPath = to.path
        //=== -1 ? to.path = "/" : to.path
        const toLogin= toPath.indexOf("/login")
        // 判断本地是否有token
        const tokenStatus = window.localStorage.getItem(CONFIG.TOKEN_NAME)
        if (toLogin === 0 && tokenStatus ){
            next("/")
        }else if (toLogin === 0 && !tokenStatus ){
            // 放行
            next()
        }else if (tokenStatus){
            next()
        }else{
            next("/login")
        }
})

// 局部首位


export default router;


// 定义用户相关的路由映射
const  userRoutes ={
    path: "/user",
    component: ()=>import('../view/layout/Layout.vue'),
    // 定义/user子路由
    // 访问/user自动跳转到list
    redirect: "/user/list",
    children: [
        {
            path: "add",
            component:()=>import('../view/users/Add.vue'),
        },
        {
            path: "list",
            component: ()=>import('../view/users/List.vue'),
        },
    ]
}



export default userRoutes
export const MENU_CONFIG = [

    // 用户管理
    {
        title: "用户管理",
        index: "/user",
        icon: "iconfont icon-yonghuguanli",
        items: [
            // 查看用户
            {
                title: "查看用户",
                index: "/user/list",
            },
            // 删除用户
            {
                title: "用户组",
                index: "/user/group",
            }
        ]
    },
    // 产品管理
    {
        title: "产品管理",
        index: "/product",
        icon: "iconfont icon-chanpinguanli",
        subMenu: [
            // 先分子菜单
            {
                title: "水产品",
                index: "/product/aquatic",
                icon: "iconfont icon-shuichanpin",
                items: [
                    //添加水产品
                    {
                        title: "添加产品",
                        index: "/product/aquatic/add",
                    },
                    // 查看水产品
                    {
                        title: "查看列表",
                        index: "/product/aquatic/list",
                    }
                ]
            },
            // 又一个分类
            {
                title: "电子产品",
                index: "/product/elec",
                icon: "iconfont icon-dingdanguanli",
                items: [
                    //添加水产品
                    {
                        title: "添加产品",
                        index: "/product/elec/add",
                    },
                    // 查看水产品
                    {
                        title: "查看列表",
                        index: "/product/elec/list",
                    }
                ]
            }
        ]
    }
]
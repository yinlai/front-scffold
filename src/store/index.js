import { defineStore } from 'pinia'

// 你可以任意命名 `defineStore()` 的返回值，但最好使用 store 的名字，同时以 `use` 开头且以 `Store` 结尾。
// (比如 `useUserStore`，`useCartStore`，`useProductStore`)
// 第一个参数是你的应用中 Store 的唯一 ID。
// 创建一个全局的容器
// 这个容器接受两个参数，第一个参数： 容器的id,第二个参数：容器的内容
//defineStore返回的是一个函数，这个函数按照userXXX去命名   counter---> useCounter
export const useIsCollapse = defineStore('isCollapse', {
    // 容器的内容
    // state: 用来存储全局状态/数据， 可以理解为数据配置的位置
    state: ()=>{
        return {
            isCollapse: false
        }
    },
    // 相当于计算属性
    getters: { },
    // 定义修改数据的方法， 相当于methods
    actions: {
        // 定义一个方法用来修改msg中的数据
        changeIsCollapse(){
            this.isCollapse = !this.isCollapse
        }
    }
})

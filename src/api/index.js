import axios from 'axios'
import {ca} from "element-plus/es/locale/index";
import {CONFIG} from "../config/index.js";
import router from "../router/index.js";
import { ElMessage } from 'element-plus'


// 添加请求拦截器
axios.interceptors.request.use(function (config) {
    // 在发送之前做些什么
    console.log("请求拦截器",config)
    if (config.method === "get"){
        // 解决缓存
        let timeStamp = (new Date()).getTime()
        if(config.params){
            // 说明本身是有params配置的
            config.params.timeStamp = timeStamp
        }else{
            config.params = {
                timeStamp: timeStamp
            }
        }
    }
    // 取出token的值
    let TokenValue = ""
    try {
        TokenValue = window.localStorage.getItem(CONFIG.TOKEN_NAME)
    }catch(error){
        TokenValue = ""
    }
    if (TokenValue == "" || TokenValue == null) {
         config.headers[CONFIG.TOKEN_NAME] = ""
    }else{
        config.headers[CONFIG.TOKEN_NAME] = TokenValue
    }

    return config
},
    function(err){
       // 对请求错误做些什么
        ElMessage({
            message: '登录失效',
            type: 'error',
        })
        return Promise.reject(err)
})


// 响应拦截器
axios.interceptors.response.use(function (response) {
    // 2xx 范围的状态码都会触发该函数
    // 对响应数据做点什么
    // 判断响应状态码是否正常
    console.log("response",response)
    if (response.data.code == 200) {
        return Promise.resolve(response)
    }else if (response.data.code === 401){
        // 说明token失效
        ElMessage({
            message: '登录失效',
            type: 'waring',
        })
        // 删除token
        window.localStorage.removeItem(CONFIG.TOKEN_NAME)
        // 跳转到登陆页面

        router.currentRoute.value.path !== "/login" && router.replace("/login")
    }
    return response
}, function(error) {
    // 超出2xx 范围的状态码都会触发该函数 对响应错误做点什么
    ElMessage({
        message: '登录失效',
        type: 'error',
    })
    return Promise.reject(error);
})

const request = (url, data = {}, method = 'GET', timeout = 5000) => {
    console.log("使用封装函数去处理请求")
    const methodLower = method.toLowerCase();
        let config = {
            method: methodLower,
            url: url,
            timeout: timeout,
            data: data
        };
    console.log(config)
    return new Promise((resolve, reject) => {
        console.log("使用axios请求接口")
        // 请求方法转成小写

        let config = {
            method: methodLower,
            url: url,
            timeout: timeout,
        };
        if (methodLower === 'get') {
            config.params = data;
        } else if (methodLower === 'post') {
            config.data = data;
        }
        axios(config)
            .then((response) => {
                // 正常拿到数据
                resolve(response)
            })
            .catch((error) => {
                // 捕获错误类型，并给出更明确的提示信息
                if (error.response) {
                    // 请求已发出，但服务器返回状态码不在 2xx 范围内
                    console.error('Response Error:', error.response.status, error.response.data);
                    reject(error.response.data);
                } else if (error.request) {
                    // 请求已发出，但没有收到响应
                    console.error('Request Error:', error.request);
                    reject('请求失败，请检查网络连接或服务是否正常');
                } else {
                    // 发生了一些错误，触发了请求的设置
                    console.error('Error:', error.message);
                    reject('请求失败，请稍后重试');
                }
            });
    });
}

export default request
import {API_CONFIG} from "../config/index.js";
import request from "./index.js";

export const  login = (username,password)=>{
    return request(API_CONFIG.loginApi,{username,password},"POST",2000)

}

// 如果是多用户操作，可以传入用户名，让后在删除
export const  logOutHandler = ()=>{
    return request(API_CONFIG.logOut,{},"GET",2000)

}

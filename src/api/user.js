import {API_CONFIG} from "../config/index.js";
import request from "./index.js";

export const  getUserListHandler = ()=>{
    return request(API_CONFIG.getUserListApi,{},"GET",2000)

}

export const  deleteUserHandler = (identity)=>{
    return request(API_CONFIG.deleteUserApi,{identity},"GET",2000)
}

// 直接接收一个对象
export const  addUserHandler = (userForm)=>{
    return request(API_CONFIG.addUserApi,userForm,"POST",2000)
}

// 直接接收一个对象
export const  updateUserHandler = (userForm)=>{
    return request(API_CONFIG.updateUserApi,userForm,"POST",2000)
}